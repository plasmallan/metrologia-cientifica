#include <Adafruit_ADS1X15.h>
#include <Vcc.h>
Adafruit_ADS1115 ads;  /* Use this for the 16-bit version */
// Adafruit_ADS1015 ads;     /* Use this for the 12-bit version */
// int milliVolts;

float c1 = 1.009249522e-03, c2 = 2.378405444e-04, c3 = 2.019202697e-07;
int16_t adc3;
float thermistor, logR_Th, vcc, volts3, resistance, curr, temp_amb;


void setup(void)
{
  Serial.begin(9600);
  Serial.println("Sensor de resistencia que utiliza la salida de LM317 como base");

  Serial.println("Rx=Vo*R/1.25");
  Serial.println("Rango en voltaje ADC: +/- 4.096 V (1 bit = 0.125 mV)");
  
  ads.setGain(GAIN_ONE);        // 1x gain   +/- 4.096V  1 bit = 0.125mV

  if (!ads.begin()) {
    Serial.println("Failed to initialize ADS.");
    while (1);
  }
}

void loop(void)
{
  vcc = Vcc::measure()/1000.0;
  temp_amb = (ads.computeVolts(ads.readADC_SingleEnded(1)))*100.0-51.30;
  adc3 = ads.readADC_SingleEnded(3);
  volts3=ads.computeVolts(adc3); // *1000.0;
  logR_Th=log(64000.0*(1023.0/analogRead(0)-1.0));
  thermistor=(1.0 / (c1 + c2*logR_Th + c3*logR_Th*logR_Th*logR_Th))-273.15;
  // milliVolts=5000;
  // curr=(milliVolts-volts3)/(64000.0);
  resistance=(volts3)*328.0/1.25-25.0;
  // resistance=resistance/1000;
  
  Serial.print("V = "); Serial.print(volts3*1000.0); Serial.print("  mV"); Serial.print("  Rx ="); Serial.print(resistance); Serial.print(" Ohm"); Serial.print("  T_amb ="); Serial.print(temp_amb); Serial.print(" °C"); Serial.print("  T_s ="); Serial.print(thermistor); Serial.print(" °C"); Serial.print("\n");

  delay(1000);
}
