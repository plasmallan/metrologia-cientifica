#include <Adafruit_ADS1X15.h>
#include <Vcc.h>
Adafruit_ADS1115 ads;  /* Use this for the 16-bit version */
// Adafruit_ADS1015 ads;     /* Use this for the 12-bit version */
// int milliVolts;

float c1 = 1.009249522e-03, c2 = 2.378405444e-04, c3 = 2.019202697e-07;
float a1 = -6.43938577, a2 = 2.47389339e-01, a3 = -1.32032813e-03, a4 = 1.82973051e-06, errorRx;
float b1 = -20.45110103, b2 = 1.65987922, b3 = -0.03246363, errorT;
float A = 32.898e-06, l = 3e-02;
int16_t adc3;
float thermistor, V_th, R_th, logR_Th, vcc, volts3, resistance, conductance, temp_amb, conductivity;

void setup(void)
{
  Serial.begin(9600);
  Serial.println("Sensor de resistencia que utiliza la salida de LM317 como base");

  Serial.println("Rx=Vo*R/1.25");
  Serial.println("Rango en voltaje ADC: +/- 4.096 V (1 bit = 0.125 mV)");
  Serial.print("kappa (S/m)  "); Serial.print("Rx (Ohm)  "); Serial.print("T_amb (°C)  "); Serial.print("T_s (°C)  "); Serial.print("\n");

  ads.setGain(GAIN_ONE);        // 1x gain   +/- 4.096V  1 bit = 0.125mV
  // ads.setGain(GAIN_TWO);        // 2x gain   +/- 2.048V  1 bit = 1mV      0.0625mV
  if (!ads.begin()) {
    Serial.println("Failed to initialize ADS.");
    while (1);
  }
}

void loop(void)
{
  vcc = Vcc::measure() / 1000.0;
  temp_amb = (ads.computeVolts(ads.readADC_SingleEnded(1))) * 100.0 - 51.30;
  adc3 = ads.readADC_SingleEnded(3);
  volts3 = ads.computeVolts(adc3); // *1000.0;
  V_th = analogRead(0) / 1023.0 * vcc;
  R_th = 64000.0 * (1023.0 / analogRead(0) - 1.0);
  logR_Th = log(R_th);
  thermistor = (1.0 / (c1 + c2 * logR_Th + c3 * logR_Th * logR_Th * logR_Th)) - 273.15;
  errorT = b1 + b2 * thermistor + b3 * thermistor * thermistor;
  thermistor = thermistor - errorT;
  // milliVolts=5000;
  // curr=(milliVolts-volts3)/(64000.0);
  resistance = (volts3) * 328.0 / 1.25 - 25.0;
  errorRx = a1 + a2 * resistance + a3 * resistance * resistance + a4 * resistance * resistance * resistance;
  resistance = resistance - errorRx;
  conductance = 1 / resistance;
  conductivity = conductance * l / A;
  Serial.print(conductivity); Serial.print(" "); Serial.print(resistance); Serial.print("  "); Serial.print(temp_amb); Serial.print(" "); Serial.print(thermistor); Serial.print("  "); Serial.print("\n");

  delay(250);
}
